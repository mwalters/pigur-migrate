package main

import (
	"database/sql"
	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
	"os"
	"time"
)

type orig struct {
	Extension string
	Full      []byte
	Thumb     []byte
}

func main() {
	con, err := sql.Open("sqlite3", "pigur.db")
	if err != nil {
		panic(err)
	}
	origs := make([]orig, 0)
	rows, err := con.Query("SELECT extension, FULL, thumb FROM images ORDER BY id ASC")
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		if rows.Err() != nil {
			panic(rows.Err())
		}
		o := orig{}
		if err := rows.Scan(&o.Extension, &o.Full, &o.Thumb); err != nil {
			panic(err)
		}
		origs = append(origs, o)
	}
	if err := rows.Close(); err != nil {
		panic(err)
	}

	tx, err := con.Begin()
	if err != nil {
		panic(err)
	}
	stmt, err := tx.Prepare("INSERT INTO images_new (id, created_at, extension, full_file, thumb) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		tx.Rollback()
		panic(err)
	}
	for _, v := range origs {
		_, err := stmt.Exec(uuid.New(), time.Now().UnixNano(), v.Extension, v.Full, v.Thumb)
		if err != nil {
			tx.Rollback()
			panic(err)
		}
		time.Sleep(1 * time.Nanosecond)
	}
	if err := tx.Commit(); err != nil {
		panic(err)
	}
	os.Exit(0)
}
